package transaccion;
import java.util.Scanner;
/**
 *
 * @author ferminpaez
 */
public class Transaccion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
        int saldo=0;
        int tt=9;
        int valor=0;
        Scanner leer = new Scanner (System.in);
        while(tt!=0){
            System.out.println("Tipo de transaccion (1=deposito, 2=retiro, 0=salir): ");
            tt=leer.nextInt();
            
            if (tt==2){
                System.out.println("Valor de transaccion: ");
                valor=leer.nextInt();
                if (valor>saldo)
                    System.out.println("Transaccion invalida. Fondos insuficientes");
                else
                    saldo=saldo-valor;
            }
            else
                if(tt!=0){
                    System.out.println("Valor de transaccion: ");
                    valor=leer.nextInt();
                    saldo=saldo+valor;
                }
                else if (tt!=0){
                    System.out.println("Tipo de transaccion invalido");
                }
        }
        System.out.println("Saldo Final: " + saldo);
    }
    
}
